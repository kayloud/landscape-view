var express = require('express')
var mongodb = require('mongodb')
var MongoClient = mongodb.MongoClient
var bodyParser = require('body-parser')
var path = require('path')
var serveStatic = require('serve-static')
var dbUrl = 'mongodb://localhost:27017'
var app = express()
var port = process.env.PORT || 5000

app.use(serveStatic(__dirname + '/dist'))
app.use(bodyParser.json())

app.listen(port)

console.log('server started ' + port)

app.get('/call', (req, res) => {
  res.send('hello world')
})

app.get('/initDb', (req, res) => {
  MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
    console.log(err)
    console.log('connected to server')
    const db = client.db('mydb')
    const collection = db.collection('customers')
    collection.createIndex( { "name": 1 }, { unique: true } )
    collection.insertMany([
      {
        'name': 'Dennis Oh',
        'street1': '1410 Countryside Dr',
        'city': 'Buffalo Grove',
        'state': 'IL',
        'zip': '60089'
      },
      {
        'name': 'Dustin Granat',
        'street1': '1420 Winston Dr',
        'city': 'Buffalo Grove',
        'state': 'IL',
        'zip': '60089'
      }
    ])

    client.close()
  })

  res.send('init success')
})

app.get('/list', (req, res) => {
  var result

  MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
    if (err) {
      console.log(err)
    }
    const db = client.db('mydb')
    const collection = db.collection('customers')
    collection.find({}).toArray(function (err, docs) {
      if (err) {
        console.log(err)
      }

      result = docs
      client.close()
      res.send(result)
    })
  })
})

app.put('/delete', (req, res) => {
  console.log(req.body)
  
  MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
    if (err) {
      console.log(err)
    }
    console.log('deleting ' + req.body.name)
    const db = client.db('mydb')
    const collection = db.collection('customers')
    var customer = req.body

    collection.deleteOne({ '_id': new mongodb.ObjectID(customer._id) })

    client.close()

    res.send('deleted')
  })
})

app.put('/add', (req, res) => {
  MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
    if (err) {
      console.log(err)
    }
    const db = client.db('mydb')
    const collection = db.collection('customers')
    var customer = req.body
    var retId

    try {
      collection.insertOne(
        {
          'name': customer.name,
          'street1': customer.street1,
          'city': customer.city,
          'state': customer.state,
          'zip': customer.zip
        }, function (err, result) {
          retId = result.insertedId
          client.close()
          res.send(retId)
        }
      )
    } catch(e) {
      console.log(e)
      client.close()
      res.status(404).send('Duplciate name')
    }
  })
})

app.get('/getCustomer', (req, res) => {
  console.log('get customer')
  console.log(req.query)
  
  MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
    if (err) {
      console.log(err)
    }
    console.log('get customer ' + req.query.id)
    const db = client.db('mydb')
    const collection = db.collection('customers')

    try {
      collection.findOne({ '_id': new mongodb.ObjectID(req.query.id) },
        function(err, result) {
          console.log(result)
          client.close()
          res.send(result)
        }
      )
    } catch(e) {
      console.log(e)
      client.close()
      res.status(404).send('Error getting customer')
    }
  })

})
