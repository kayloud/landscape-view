import Vue from 'vue'
import Router from 'vue-router'
import ListView from '@/components/ListView'
import MapView from '@/components/MapView'
import CustomerView from '@/components/CustomerView'
import AddCustomer from '@/components/AddCustomer'
import Dashboard from '@/components/Dashboard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/list',
      name: 'ListView',
      component: ListView,
      props: true
    },
    {
      path: '/map',
      name: 'MapView',
      component: MapView
    },
    {
      path: '/customer/:id',
      name: 'CustomerView',
      component: CustomerView
    },
    {
      path: '/addcustomer/',
      name: 'AddCustomer',
      component: AddCustomer
    },
    {
      path: '/addcustomer/:id',
      name: 'AddCustomer',
      component: AddCustomer
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    }
  ]
})
